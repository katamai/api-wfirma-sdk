<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Messages\Invoices\Contracts;

use Katamai\wFirmaSdk\Messages\Commons\Invoice;
use Katamai\wFirmaSdk\Messages\Contracts\MessageInterface;

interface AddInvoiceMessageInterface extends MessageInterface
{
    public function addInvoice(Invoice $invoice): self;

    public function getInvoices(): array;

    public function setInvoices(array $invoices): void;
}
