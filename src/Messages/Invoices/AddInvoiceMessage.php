<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Messages\Invoices;

use Katamai\wFirmaSdk\Messages\Commons\Invoice;
use Katamai\wFirmaSdk\Messages\Invoices\Contracts\AddInvoiceMessageInterface;

class AddInvoiceMessage implements AddInvoiceMessageInterface
{
    /**
     * @var \Katamai\wFirmaSdk\Messages\Commons\Invoice[]
     */
    protected array $invoices;

    public function __construct()
    {
    }

    public function addInvoice(Invoice $invoice): self
    {
        $this->invoices[] = $invoice;

        return $this;
    }

    /**
     * @return \Katamai\wFirmaSdk\Messages\Commons\Invoice[]
     */
    public function getInvoices(): array
    {
        return $this->invoices;
    }

    /**
     * @param \Katamai\wFirmaSdk\Messages\Commons\Invoice[] $invoices
     */
    public function setInvoices(array $invoices): void
    {
        $this->invoices = $invoices;
    }

    public function toArray(): array
    {
        $output = [];

        foreach ($this->invoices as $key => $invoice) {
            $output['invoices'][$key]['invoice'] = $invoice->toArray();
        }

        return $output;
    }
}
