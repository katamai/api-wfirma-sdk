<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Messages\Contracts;

interface ToArray
{
    public function toArray(): array;
}
