<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Messages\Contracts;

interface MessageInterface extends ToArray
{
}
