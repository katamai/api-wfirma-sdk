<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Messages\Commons\Contracts;

interface InvoiceInterface
{
    public const TYPE_NORMAL = 'normal';
}
