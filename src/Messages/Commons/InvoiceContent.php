<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Messages\Commons;

use Katamai\wFirmaSdk\Messages\Contracts\ToArray;

class InvoiceContent implements ToArray
{
    private string $name;

    private float $count;

    private float $unitCount;

    private float $price;

    private string $unit;

    private string $vatRate;

    /**
     * @param string $name
     * @param float  $count
     * @param float  $unitCount
     * @param float  $price
     * @param string $unit
     * @param string $vatRate
     */
    public function __construct(string $name, float $count, float $unitCount, float $price, string $unit, string $vatRate)
    {
        $this->name = $name;
        $this->count = $count;
        $this->unitCount = $unitCount;
        $this->price = $price;
        $this->unit = $unit;
        $this->vatRate = $vatRate;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getCount(): float
    {
        return $this->count;
    }

    /**
     * @return float
     */
    public function getUnitCount(): float
    {
        return $this->unitCount;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getUnit(): string
    {
        return $this->unit;
    }

    /**
     * @return string
     */
    public function getVatRate(): string
    {
        return $this->vatRate;
    }

    public function toArray(): array
    {
        return [
            'name'       => $this->name,
            'count'      => $this->count,
            'unit_count' => $this->unitCount,
            'price'      => $this->price,
            'unit'       => $this->unit,
            'vat'        => $this->vatRate,
        ];
    }
}
