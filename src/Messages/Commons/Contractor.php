<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Messages\Commons;

use Katamai\wFirmaSdk\Messages\Contracts\ToArray;

class Contractor implements ToArray
{
    private string $name;

    private string $street;

    private string $zipCode;

    private string $city;

    private string $country;

    private ?string $nip;

    private ?string $phone;

    private ?string $email;

    /**
     * @param string  $name
     * @param string  $street
     * @param string  $zipCode
     * @param string  $city
     * @param string  $country
     * @param ?string $nip
     * @param ?string $phone
     * @param ?string $email
     */
    public function __construct(
        string $name,
        string $street,
        string $zipCode,
        string $city,
        string $country,
        ?string $nip = null,
        ?string $phone = null,
        ?string $email = null
    ) {
        $this->name = $name;
        $this->street = $street;
        $this->zipCode = $zipCode;
        $this->city = $city;
        $this->country = $country;
        $this->nip = $nip;
        $this->phone = $phone;
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return string|null
     */
    public function getNip(): ?string
    {
        return $this->nip;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    public function toArray(): array
    {
        return [
            'name'    => $this->name,
            'nip'     => $this->nip,
            'street'  => $this->street,
            'zip'     => $this->zipCode,
            'city'    => $this->city,
            'country' => $this->country,
            'phone'   => $this->phone,
            'email'   => $this->email,
        ];
    }
}
