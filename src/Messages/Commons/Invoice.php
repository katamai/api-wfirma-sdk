<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Messages\Commons;

use Carbon\Carbon;
use Katamai\wFirmaSdk\Messages\Commons\Contracts\InvoiceInterface;
use Katamai\wFirmaSdk\Messages\Contracts\ToArray;

class Invoice implements InvoiceInterface, ToArray
{
    private string $type;

    private Contractor $contractor;

    /**
     * @var \Katamai\wFirmaSdk\Messages\Commons\InvoiceContent[]
     */
    private array $invoiceContents;

    private string $currency;

    /**
     * @var string|null
     */
    private $externalId;

    private ?string $translationLanguageId;

    private ?Carbon $paymentDate;

    private ?bool $paid;

    public function __construct(
        string $type,
        Contractor $contractor,
        ?string $translationLanguageId = null,
        ?Carbon $paymentDate = null,
        string $currency = 'PLN',
        ?string $externalId = null,
        ?bool $paid = false
    ) {
        $this->type = $type;
        $this->contractor = $contractor;
        $this->currency = $currency;
        $this->externalId = $externalId;
        $this->translationLanguageId = $translationLanguageId;
        $this->paymentDate = $paymentDate;
        $this->paid = $paid;
    }

    public function addInvoiceContents(InvoiceContent $invoiceContent): self
    {
        $this->invoiceContents[] = $invoiceContent;

        return $this;
    }

    /**
     * @return array
     */
    public function getInvoiceContents(): array
    {
        return $this->invoiceContents;
    }

    /**
     * @param array $invoiceContents
     */
    public function setInvoiceContents(array $invoiceContents): void
    {
        $this->invoiceContents = $invoiceContents;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return \Katamai\wFirmaSdk\Messages\Commons\Contractor
     */
    public function getContractor(): Contractor
    {
        return $this->contractor;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @return string|null
     */
    public function getTranslationLanguageId(): ?string
    {
        return $this->translationLanguageId;
    }

    /**
     * @return \Carbon\Carbon|null
     */
    public function getPaymentDate(): ?Carbon
    {
        return $this->paymentDate;
    }

    /**
     * @return bool|null
     */
    public function getPaid(): ?bool
    {
        return $this->paid;
    }

    public function toArray(): array
    {
        $output = [
            'type'        => $this->type,
            'currency'    => $this->currency,
            'id_external' => $this->externalId,
            'contractor'  => $this->contractor->toArray(),
        ];

        if (null !== $this->translationLanguageId) {
            $output['translation_language_id'] = $this->translationLanguageId;
        }

        if (null !== $this->paymentDate) {
            $output['paymentdate'] = $this->paymentDate->format('Y-m-d');
        }

        if ($this->paid) {
            $output['paid'] = 1;
        }

        foreach ($this->invoiceContents as $key => $invoiceContent) {
            $output['invoicecontents'][$key]['invoicecontent'] = $invoiceContent->toArray();
        }

        return $output;
    }
}
