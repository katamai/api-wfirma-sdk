<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Sessions;

use Katamai\wFirmaSdk\Sessions\Contracts\SessionInterface;
use Psr\Http\Message\RequestInterface;

class BasicAuthSession implements SessionInterface
{
    private string $username;

    private string $password;

    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function authorizeRequest(RequestInterface $request): RequestInterface
    {
        return $request->withHeader('Authorization', sprintf('Basic %s', base64_encode(sprintf('%s:%s', $this->username, $this->password))));
    }
}
