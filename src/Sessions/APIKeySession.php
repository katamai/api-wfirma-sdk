<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Sessions;

use Katamai\wFirmaSdk\Sessions\Contracts\SessionInterface;
use Psr\Http\Message\RequestInterface;

class APIKeySession implements SessionInterface
{
    private string $accessKey;

    private string $secretKey;

    private string $appKey;

    public function __construct(string $accessKey, string $secretKey, string $appKey)
    {
        $this->accessKey = $accessKey;
        $this->secretKey = $secretKey;
        $this->appKey = $appKey;
    }

    public function getAccessKey(): string
    {
        return $this->accessKey;
    }

    public function getSecretKey(): string
    {
        return $this->secretKey;
    }

    public function getAppKey(): string
    {
        return $this->appKey;
    }

    public function authorizeRequest(RequestInterface $request): RequestInterface
    {
        $request = $request->withHeader('accessKey', $this->getAccessKey());
        $request = $request->withHeader('secretKey', $this->getSecretKey());
        $request = $request->withHeader('appKey', $this->getAppKey());

        return $request;
    }
}
