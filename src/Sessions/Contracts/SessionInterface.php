<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Sessions\Contracts;

use Psr\Http\Message\RequestInterface;

interface SessionInterface
{
    public function authorizeRequest(RequestInterface $request): RequestInterface;
}
