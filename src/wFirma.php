<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk;

use Katamai\wFirmaSdk\Connections\Contracts\ConnectionInterface;
use Katamai\wFirmaSdk\Factories\Contracts\RequestFactoryInterface;
use Katamai\wFirmaSdk\Factories\Contracts\TransformersFactoryInterface;
use Katamai\wFirmaSdk\Messages\Invoices\Contracts\AddInvoiceMessageInterface;
use Katamai\wFirmaSdk\Models\Responses\UserCompanies\Contracts\GetUserCompaniesResponseInterface;
use Katamai\wFirmaSdk\Sessions\Contracts\SessionInterface;
use Psr\Http\Message\ResponseInterface;

class wFirma
{
    private ConnectionInterface $connection;

    private SessionInterface $session;

    private RequestFactoryInterface $requestFactory;

    private TransformersFactoryInterface $transformersFactory;

    public function __construct(
        ConnectionInterface $connection,
        SessionInterface $session,
        RequestFactoryInterface $requestFactory,
        TransformersFactoryInterface $transformersFactory
    ) {
        $this->connection = $connection;
        $this->session = $session;
        $this->requestFactory = $requestFactory;
        $this->transformersFactory = $transformersFactory;
    }

    public function getUserCompanies(): GetUserCompaniesResponseInterface
    {
        return $this->transformersFactory->createGetUserCompaniesResponseTransformer()
            ->transform(
                $this->connection->send(
                    $this->session->authorizeRequest(
                        $this->requestFactory->createGetUserCompanies()
                    )
                )
            );
    }

    public function addInvoice(int $companyId, AddInvoiceMessageInterface $addInvoiceMessage): ResponseInterface
    {
        return $this->connection->send(
            $this->session->authorizeRequest(
                $this->requestFactory->createAddInvoice($companyId, $addInvoiceMessage)
            )
        );
    }

    public function downloadInvoice(int $invoiceId, int $companyId): ResponseInterface
    {
        return $this->connection->send(
            $this->session->authorizeRequest(
                $this->requestFactory->createDownloadInvoice($invoiceId, $companyId)
            )
        );
    }
}
