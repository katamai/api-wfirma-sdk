<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Extensions;

use Assert\Assertion;

class wFirmaAssertion extends Assertion
{
    protected static $exceptionClass = \InvalidArgumentException::class;
}
