<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Extensions\Contracts;

use Katamai\wFirmaSdk\Transformers\Contracts\TransformerInterface;

interface CollectionTransformerInterface
{
    /**
     * @param iterable                                                       $collection
     * @param \Katamai\wFirmaSdk\Transformers\Contracts\TransformerInterface $transformer
     *
     * @return array
     */
    public function transform(iterable $collection, TransformerInterface $transformer): array;
}
