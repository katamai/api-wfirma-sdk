<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Extensions;

use Katamai\wFirmaSdk\Extensions\Contracts\CollectionTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Contracts\TransformerInterface;

class CollectionTransformer implements CollectionTransformerInterface
{
    /**
     * @inheritDoc
     */
    public function transform(iterable $collection, TransformerInterface $transformer): array
    {
        $transformedCollection = [];

        foreach ($collection as $item) {
            $transformedCollection[] = $transformer->transform($item);
        }

        return $transformedCollection;
    }
}
