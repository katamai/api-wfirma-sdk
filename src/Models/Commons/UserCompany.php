<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Models\Commons;

use Katamai\wFirmaSdk\Models\Commons\Contracts\CompanyInterface;
use Katamai\wFirmaSdk\Models\Commons\Contracts\UserCompanyInterface;

class UserCompany implements UserCompanyInterface
{
    private string $id;

    private string $right;

    private int $userId;

    private int $warehouseId;

    private CompanyInterface $company;

    private array $companyPack;

    /**
     * @param array $data
     */
    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->right = $data['right'];
        $this->userId = $data['user']['id'];
        $this->warehouseId = $data['warehouse']['id'];
        $this->company = $data['company'];
        $this->companyPack = $data['company_pack'];
    }

    /**
     * @return mixed|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed|string
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * @return int|mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return int|mixed
     */
    public function getWarehouseId()
    {
        return $this->warehouseId;
    }

    /**
     * @return mixed|\stdClass
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return mixed|\stdClass
     */
    public function getCompanyPack()
    {
        return $this->companyPack;
    }
}
