<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Models\Commons\Contracts;

interface PaginationInterface
{
    public function getLimit(): int;

    public function getPage(): int;

    public function getTotal(): int;
}
