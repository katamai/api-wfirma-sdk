<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Models\Commons;

use Katamai\wFirmaSdk\Models\Commons\Contracts\PaginationInterface;

class Pagination implements PaginationInterface
{
    private int $limit;

    private int $page;

    private int $total;

    public function __construct(array $data)
    {
        $this->limit = (int) $data['limit'];
        $this->page = (int) $data['page'];
        $this->total = (int) $data['total'];
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }
}
