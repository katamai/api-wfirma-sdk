<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Models\Commons;

use Katamai\wFirmaSdk\Models\Commons\Contracts\StatusInterface;

class Status implements StatusInterface
{
    private string $code;

    public function __construct(array $data)
    {
        $this->code = $data['code'];
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
}
