<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Models\Commons;

use Katamai\wFirmaSdk\Models\Commons\Contracts\CompanyInterface;

class Company implements CompanyInterface
{
    private string $id;

    private string $isAccountant;

    private string $isVerified;

    private string $name;

    private string $altName;

    private string $nip;

    private string $bdo;

    private string $warehouseType;

    private string $vatPayer;

    private string $tax;

    private string $taxLumpDefaultRate;

    private string $bookStartDate;

    private string $isRegistered;

    public function __construct(array $data)
    {
        $this->id = $data['id'];
        $this->isAccountant = $data['is_accountant'];
        $this->isVerified = $data['is_verified'];
        $this->name = $data['name'];
        $this->altName = $data['altname'];
        $this->nip = $data['nip'];
        $this->bdo = $data['bdo'];
        $this->warehouseType = $data['warehouse_type'];
        $this->vatPayer = $data['vat_payer'];
        $this->tax = $data['tax'];
        $this->taxLumpDefaultRate = $data['tax_lump_default_rate'];
        $this->bookStartDate = $data['book_start_date'];
        $this->isRegistered = $data['is_registered'];
    }

    /**
     * @return mixed|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed|string
     */
    public function getIsAccountant()
    {
        return $this->isAccountant;
    }

    /**
     * @return mixed|string
     */
    public function getIsVerified()
    {
        return $this->isVerified;
    }

    /**
     * @return mixed|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed|string
     */
    public function getAltName()
    {
        return $this->altName;
    }

    /**
     * @return mixed|string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * @return mixed|string
     */
    public function getBdo()
    {
        return $this->bdo;
    }

    /**
     * @return mixed|string
     */
    public function getWarehouseType()
    {
        return $this->warehouseType;
    }

    /**
     * @return mixed|string
     */
    public function getVatPayer()
    {
        return $this->vatPayer;
    }

    /**
     * @return mixed|string
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @return mixed|string
     */
    public function getTaxLumpDefaultRate()
    {
        return $this->taxLumpDefaultRate;
    }

    /**
     * @return mixed|string
     */
    public function getBookStartDate()
    {
        return $this->bookStartDate;
    }

    /**
     * @return mixed|string
     */
    public function getIsRegistered()
    {
        return $this->isRegistered;
    }
}
