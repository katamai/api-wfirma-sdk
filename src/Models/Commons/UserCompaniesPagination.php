<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Models\Commons;

use Katamai\wFirmaSdk\Models\Commons\Contracts\PaginationInterface;
use Katamai\wFirmaSdk\Models\Commons\Contracts\UserCompaniesPaginationInterface;

class UserCompaniesPagination implements UserCompaniesPaginationInterface
{
    /**
     * @var \Katamai\wFirmaSdk\Models\Commons\UserCompany[]
     */
    private array $userCompanies;

    private PaginationInterface $parameters;

    /**
     * @param array $data
     */
    public function __construct($data)
    {
        $this->userCompanies = $data['data'];
        $this->parameters = $data['parameters'];
    }

    public function getUserCompanies(): array
    {
        return $this->userCompanies;
    }

    /**
     * @return \Katamai\wFirmaSdk\Models\Commons\Contracts\PaginationInterface
     */
    public function getParameters(): PaginationInterface
    {
        return $this->parameters;
    }
}
