<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Models\Responses\UserCompanies;

use Katamai\wFirmaSdk\Models\Commons\Contracts\StatusInterface;
use Katamai\wFirmaSdk\Models\Commons\UserCompaniesPagination;
use Katamai\wFirmaSdk\Models\Responses\UserCompanies\Contracts\GetUserCompaniesResponseInterface;

class GetUserCompaniesResponse implements GetUserCompaniesResponseInterface
{
    private UserCompaniesPagination $userCompanies;

    private StatusInterface $status;

    /**
     * @param array $data
     */
    public function __construct($data)
    {
        $this->userCompanies = $data['user_companies'];
        $this->status = $data['status'];
    }

    /**
     * @return \Katamai\wFirmaSdk\Models\Commons\UserCompaniesPagination
     */
    public function getUserCompanies(): UserCompaniesPagination
    {
        return $this->userCompanies;
    }

    /**
     * @return \Katamai\wFirmaSdk\Models\Commons\Contracts\StatusInterface
     */
    public function getStatus(): StatusInterface
    {
        return $this->status;
    }
}
