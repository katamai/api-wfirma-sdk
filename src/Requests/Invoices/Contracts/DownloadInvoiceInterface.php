<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Requests\Invoices\Contracts;

use Katamai\wFirmaSdk\Requests\Contracts\RequestInterface;

interface DownloadInvoiceInterface extends RequestInterface
{
}
