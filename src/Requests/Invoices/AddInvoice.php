<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Requests\Invoices;

use Katamai\wFirmaSdk\Messages\Invoices\Contracts\AddInvoiceMessageInterface;
use Katamai\wFirmaSdk\Requests\AbstractRequest;
use Katamai\wFirmaSdk\Requests\Invoices\Contracts\AddInvoiceInterface;

final class AddInvoice extends AbstractRequest implements AddInvoiceInterface
{
    public function __construct(int $companyId, AddInvoiceMessageInterface $addInvoiceMessage)
    {
        parent::__construct('POST', sprintf('invoices/add?%s', http_build_query([
            'inputFormat'  => 'json',
            'outputFormat' => 'json',
            'company_id'   => $companyId,
        ])), [], json_encode($addInvoiceMessage->toArray(), JSON_THROW_ON_ERROR));
    }
}
