<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Requests\Invoices;

use Katamai\wFirmaSdk\Requests\AbstractRequest;
use Katamai\wFirmaSdk\Requests\Invoices\Contracts\DownloadInvoiceInterface;

class DownloadInvoice extends AbstractRequest implements DownloadInvoiceInterface
{
    public function __construct(int $invoiceId, int $companyId)
    {
        parent::__construct('GET', sprintf('invoices/download/%d?%s', $invoiceId, http_build_query([
            'inputFormat'  => 'json',
            'outputFormat' => 'json',
            'company_id'   => $companyId,
        ])));
    }
}
