<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Requests\UserCompanies;

use Katamai\wFirmaSdk\Requests\AbstractRequest;
use Katamai\wFirmaSdk\Requests\UserCompanies\Contracts\GetUserCompaniesInterface;

final class GetUserCompanies extends AbstractRequest implements GetUserCompaniesInterface
{
    /**
     */
    public function __construct()
    {
        parent::__construct('GET', sprintf('user_companies/find?%s', http_build_query([
            'inputFormat'  => 'json',
            'outputFormat' => 'json',
        ])));
    }
}
