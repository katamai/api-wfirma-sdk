<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Requests\UserCompanies\Contracts;

use Katamai\wFirmaSdk\Requests\Contracts\RequestInterface;

interface GetUserCompaniesInterface extends RequestInterface
{
}
