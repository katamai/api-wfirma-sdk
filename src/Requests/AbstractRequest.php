<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Requests;

use GuzzleHttp\Psr7\Request;
use Katamai\wFirmaSdk\Requests\Contracts\RequestInterface;

abstract class AbstractRequest extends Request implements RequestInterface
{
    /**
     * @param string                                                 $method  HTTP method
     * @param \Psr\Http\Message\UriInterface|string                  $uri     URI
     * @param array                                                  $headers Request headers
     * @param \Psr\Http\Message\StreamInterface|resource|string|null $body    Request body
     * @param string                                                 $version Protocol version
     */
    public function __construct($method, $uri, array $headers = [], $body = null, $version = '1.1')
    {
        parent::__construct($method, $uri, $headers + [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json',
            ], $body, $version);
    }
}
