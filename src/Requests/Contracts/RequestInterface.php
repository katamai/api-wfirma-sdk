<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Requests\Contracts;

use Psr\Http\Message\RequestInterface as PsrRequestInterface;

interface RequestInterface extends PsrRequestInterface
{
}
