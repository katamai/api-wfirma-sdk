<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Requests\UserCompanies;

use Katamai\wFirmaSdk\Requests\AbstractRequest;

final class GetCompanies extends AbstractRequest
{
    public function __construct()
    {
    }
}
