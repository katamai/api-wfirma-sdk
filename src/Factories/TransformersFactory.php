<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Factories;

use Katamai\wFirmaSdk\Extensions\CollectionTransformer;
use Katamai\wFirmaSdk\Extensions\Contracts\CollectionTransformerInterface;
use Katamai\wFirmaSdk\Factories\Contracts\TransformersFactoryInterface;
use Katamai\wFirmaSdk\Transformers\Commons\CompanyTransformer;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\CompanyTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\PaginationTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\StatusTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\UserCompaniesPaginationTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\UserCompanyTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Commons\PaginationTransformer;
use Katamai\wFirmaSdk\Transformers\Commons\StatusTransformer;
use Katamai\wFirmaSdk\Transformers\Commons\UserCompaniesPaginationTransformer;
use Katamai\wFirmaSdk\Transformers\Commons\UserCompanyTransformer;
use Katamai\wFirmaSdk\Transformers\Responses\UserCompanies\Contracts\GetUserCompaniesResponseTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Responses\UserCompanies\GetUserCompaniesResponseResponseTransformer;

class TransformersFactory implements TransformersFactoryInterface
{
    public function createStatusTransformer(): StatusTransformerInterface
    {
        return new StatusTransformer();
    }

    public function createPaginationTransformer(): PaginationTransformerInterface
    {
        return new PaginationTransformer();
    }

    public function createCompanyTransformer(): CompanyTransformerInterface
    {
        return new CompanyTransformer();
    }

    public function createUserCompaniesPaginationTransformer(): UserCompaniesPaginationTransformerInterface
    {
        return new UserCompaniesPaginationTransformer();
    }

    public function createCollectionTransformer(): CollectionTransformerInterface
    {
        return new CollectionTransformer();
    }

    public function createUserCompanyTransformer(): UserCompanyTransformerInterface
    {
        return new UserCompanyTransformer(
            $this->createCompanyTransformer()
        );
    }

    public function createGetUserCompaniesResponseTransformer(): GetUserCompaniesResponseTransformerInterface
    {
        return new GetUserCompaniesResponseResponseTransformer(
            $this->createStatusTransformer(),
            $this->createPaginationTransformer(),
            $this->createCollectionTransformer(),
            $this->createUserCompanyTransformer(),
            $this->createUserCompaniesPaginationTransformer()
        );
    }
}
