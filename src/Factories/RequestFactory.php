<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Factories;

use Katamai\wFirmaSdk\Factories\Contracts\RequestFactoryInterface;
use Katamai\wFirmaSdk\Messages\Invoices\Contracts\AddInvoiceMessageInterface;
use Katamai\wFirmaSdk\Requests\Invoices\AddInvoice;
use Katamai\wFirmaSdk\Requests\Invoices\Contracts\AddInvoiceInterface;
use Katamai\wFirmaSdk\Requests\Invoices\Contracts\DownloadInvoiceInterface;
use Katamai\wFirmaSdk\Requests\Invoices\DownloadInvoice;
use Katamai\wFirmaSdk\Requests\UserCompanies\Contracts\GetUserCompaniesInterface;
use Katamai\wFirmaSdk\Requests\UserCompanies\GetUserCompanies;

class RequestFactory implements RequestFactoryInterface
{
    public function createGetUserCompanies(): GetUserCompaniesInterface
    {
        return new GetUserCompanies();
    }

    public function createAddInvoice(int $companyId, AddInvoiceMessageInterface $addInvoiceMessage): AddInvoiceInterface
    {
        return new AddInvoice($companyId, $addInvoiceMessage);
    }

    public function createDownloadInvoice(int $invoiceId, int $companyId): DownloadInvoiceInterface
    {
        return new DownloadInvoice($invoiceId, $companyId);
    }
}
