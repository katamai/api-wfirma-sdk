<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Factories;

use Katamai\wFirmaSdk\Factories\Contracts\MessageFactoryInterface;
use Katamai\wFirmaSdk\Messages\Invoices\AddInvoiceMessage;
use Katamai\wFirmaSdk\Messages\Invoices\Contracts\AddInvoiceMessageInterface;

class MessageFactory implements MessageFactoryInterface
{
    public function createMessageFactory(): AddInvoiceMessageInterface
    {
        return new AddInvoiceMessage();
    }
}
