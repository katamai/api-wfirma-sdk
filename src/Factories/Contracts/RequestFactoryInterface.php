<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Factories\Contracts;

use Katamai\wFirmaSdk\Messages\Invoices\Contracts\AddInvoiceMessageInterface;
use Katamai\wFirmaSdk\Requests\Invoices\Contracts\AddInvoiceInterface;
use Katamai\wFirmaSdk\Requests\Invoices\Contracts\DownloadInvoiceInterface;
use Katamai\wFirmaSdk\Requests\UserCompanies\Contracts\GetUserCompaniesInterface;

interface RequestFactoryInterface
{
    public function createGetUserCompanies(): GetUserCompaniesInterface;

    public function createAddInvoice(int $companyId, AddInvoiceMessageInterface $addInvoiceMessage): AddInvoiceInterface;

    public function createDownloadInvoice(int $invoiceId, int $companyId): DownloadInvoiceInterface;
}
