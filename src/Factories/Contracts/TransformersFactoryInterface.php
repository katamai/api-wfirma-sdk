<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Factories\Contracts;

use Katamai\wFirmaSdk\Transformers\Responses\UserCompanies\Contracts\GetUserCompaniesResponseTransformerInterface;

interface TransformersFactoryInterface
{
    public function createGetUserCompaniesResponseTransformer(): GetUserCompaniesResponseTransformerInterface;
}
