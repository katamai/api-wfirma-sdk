<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Factories\Contracts;

use Katamai\wFirmaSdk\Messages\Invoices\Contracts\AddInvoiceMessageInterface;

interface MessageFactoryInterface
{
    public function createMessageFactory(): AddInvoiceMessageInterface;
}
