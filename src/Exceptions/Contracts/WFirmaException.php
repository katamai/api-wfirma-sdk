<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Exceptions\Contracts;

use Throwable;

interface WFirmaException extends Throwable
{
}
