<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Exceptions;

use Katamai\wFirmaSdk\Exceptions\Contracts\WFirmaException;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use Throwable;

final class BadGatewayException extends RuntimeException implements WFirmaException
{
    /**
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param \Throwable|null                     $previous
     *
     * @throws \JsonException
     */
    public function __construct(ResponseInterface $response, ?Throwable $previous = null)
    {
        $statusCode = $response->getStatusCode();
        $body = $response->getBody();

        if ($body->isReadable()) {
            if ('application/json' === $response->getHeaderLine('Content-Type')) {
                $data = json_decode($body->getContents(), true, 512, JSON_THROW_ON_ERROR);
                $message = $data['message'] ?? $data['error'] ?? null;
            } else {
                $message = $body->getContents();
            }
        } else {
            $message = null;
        }

        parent::__construct(
            sprintf('The wFirma service resulted in %d HTTP status code: %s', $statusCode, $message ?? $response->getReasonPhrase()),
            $statusCode,
            $previous
        );
    }
}
