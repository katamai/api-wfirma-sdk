<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Connections\Contracts;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface ConnectionInterface
{
    public function send(RequestInterface $request): ResponseInterface;
}
