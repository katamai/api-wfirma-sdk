<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Connections;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Katamai\wFirmaSdk\Connections\Contracts\ConnectionInterface;
use Katamai\wFirmaSdk\Exceptions\BadGatewayException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class GuzzleConnection implements ConnectionInterface
{
    private ClientInterface $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function send(RequestInterface $request): ResponseInterface
    {
        try {
            $response = $this->client->send($request);
        } catch (RequestException $exception) {
            $exceptionResponse = $exception->getResponse();

            if (null === $exceptionResponse) {
                throw $exception;
            }

            throw new BadGatewayException($exceptionResponse, $exception);
        } catch (GuzzleException $exception) {
            // TODO: handle exception
            throw $exception;
        }

        $statusCode = $response->getStatusCode();

        if ($statusCode < 200 || $statusCode >= 300) {
            throw new BadGatewayException($response);
        }

        return $response;
    }
}
