<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Contracts;

interface TransformerInterface
{
    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function transform($data);
}
