<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Responses\UserCompanies\Contracts;

use Katamai\wFirmaSdk\Transformers\Contracts\TransformerInterface;

interface GetUserCompaniesResponseTransformerInterface extends TransformerInterface
{
    /**
     * @param mixed $data
     *
     * @return mixed
     */
    public function transform($data);
}
