<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Responses\UserCompanies;

use Katamai\wFirmaSdk\Extensions\Contracts\CollectionTransformerInterface;
use Katamai\wFirmaSdk\Extensions\wFirmaAssertion;
use Katamai\wFirmaSdk\Models\Responses\UserCompanies\GetUserCompaniesResponse;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\PaginationTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\StatusTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\UserCompaniesPaginationTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\UserCompanyTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Responses\UserCompanies\Contracts\GetUserCompaniesResponseTransformerInterface;
use Psr\Http\Message\ResponseInterface;

class GetUserCompaniesResponseResponseTransformer implements GetUserCompaniesResponseTransformerInterface
{
    private StatusTransformerInterface $statusTransformer;

    private PaginationTransformerInterface $paginationTransformer;

    private CollectionTransformerInterface $collectionTransformer;

    private UserCompanyTransformerInterface $userCompanyTransformer;

    private UserCompaniesPaginationTransformerInterface $userCompaniesPaginationTransformer;

    public function __construct(
        StatusTransformerInterface $statusTransformer,
        PaginationTransformerInterface $paginationTransformer,
        CollectionTransformerInterface $collectionTransformer,
        UserCompanyTransformerInterface $userCompanyTransformer,
        UserCompaniesPaginationTransformerInterface $userCompaniesPaginationTransformer
    ) {
        $this->statusTransformer = $statusTransformer;
        $this->paginationTransformer = $paginationTransformer;
        $this->collectionTransformer = $collectionTransformer;
        $this->userCompanyTransformer = $userCompanyTransformer;
        $this->userCompaniesPaginationTransformer = $userCompaniesPaginationTransformer;
    }

    /**
     * @param \Psr\Http\Message\ResponseInterface $data
     *
     * @throws \Assert\AssertionFailedException
     */
    public function transform($data)
    {
        wFirmaAssertion::isInstanceOf($data, ResponseInterface::class);

        $content = $data->getBody()->getContents();

        $data = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

        $data['status'] = $this->statusTransformer->transform($data['status']);

        wFirmaAssertion::isArray($data['user_companies']);

        wFirmaAssertion::keyExists($data['user_companies'], 'parameters');

        $userCompanies = $data['user_companies'];

        unset($data['user_companies']);

        $data['user_companies']['parameters'] = $this->paginationTransformer->transform($userCompanies['parameters']);
        $data['user_companies']['data'] = $this->collectionTransformer->transform(array_diff_key($userCompanies, array_flip(['parameters'])), $this->userCompanyTransformer);

        $data['user_companies'] = $this->userCompaniesPaginationTransformer->transform($data['user_companies']);

        return new GetUserCompaniesResponse($data);
    }
}
