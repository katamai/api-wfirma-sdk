<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Commons;

use Katamai\wFirmaSdk\Extensions\wFirmaAssertion;
use Katamai\wFirmaSdk\Models\Commons\Contracts\UserCompaniesPaginationInterface;
use Katamai\wFirmaSdk\Models\Commons\UserCompaniesPagination;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\UserCompaniesPaginationTransformerInterface;

class UserCompaniesPaginationTransformer implements UserCompaniesPaginationTransformerInterface
{
    public function transform($data): UserCompaniesPaginationInterface
    {
        wFirmaAssertion::isArray($data);

        return new UserCompaniesPagination($data);
    }
}
