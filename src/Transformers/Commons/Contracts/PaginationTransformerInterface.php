<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Commons\Contracts;

use Katamai\wFirmaSdk\Models\Commons\Contracts\PaginationInterface;
use Katamai\wFirmaSdk\Transformers\Contracts\TransformerInterface;

interface PaginationTransformerInterface extends TransformerInterface
{
    /**
     * @param mixed $data
     *
     * @return \Katamai\wFirmaSdk\Models\Commons\Contracts\PaginationInterface
     */
    public function transform($data): PaginationInterface;
}
