<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Commons\Contracts;

use Katamai\wFirmaSdk\Models\Commons\Contracts\StatusInterface;
use Katamai\wFirmaSdk\Transformers\Contracts\TransformerInterface;

interface StatusTransformerInterface extends TransformerInterface
{
    /**
     * @param mixed $data
     *
     * @return \Katamai\wFirmaSdk\Models\Commons\Contracts\StatusInterface
     */
    public function transform($data): StatusInterface;
}
