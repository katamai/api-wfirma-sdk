<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Commons\Contracts;

use Katamai\wFirmaSdk\Models\Commons\Contracts\CompanyInterface;

interface CompanyTransformerInterface extends CompanyInterface
{
    /**
     * @param mixed $data
     *
     * @return \Katamai\wFirmaSdk\Models\Commons\Contracts\CompanyInterface
     */
    public function transform($data): CompanyInterface;
}
