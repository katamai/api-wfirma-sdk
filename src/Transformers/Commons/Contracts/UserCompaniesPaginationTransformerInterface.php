<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Commons\Contracts;

use Katamai\wFirmaSdk\Models\Commons\Contracts\UserCompaniesPaginationInterface;
use Katamai\wFirmaSdk\Transformers\Contracts\TransformerInterface;

interface UserCompaniesPaginationTransformerInterface extends TransformerInterface
{
    /**
     * @param mixed $data
     *
     * @return \Katamai\wFirmaSdk\Models\Commons\Contracts\UserCompaniesPaginationInterface
     */
    public function transform($data): UserCompaniesPaginationInterface;
}
