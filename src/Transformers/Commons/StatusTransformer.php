<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Commons;

use Katamai\wFirmaSdk\Extensions\wFirmaAssertion;
use Katamai\wFirmaSdk\Models\Commons\Contracts\StatusInterface;
use Katamai\wFirmaSdk\Models\Commons\Status;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\StatusTransformerInterface;

class StatusTransformer implements StatusTransformerInterface
{
    public function transform($data): StatusInterface
    {
        wFirmaAssertion::isArray($data);

        return new Status($data);
    }
}
