<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Commons;

use Katamai\wFirmaSdk\Extensions\wFirmaAssertion;
use Katamai\wFirmaSdk\Models\Commons\Contracts\UserCompanyInterface;
use Katamai\wFirmaSdk\Models\Commons\UserCompany;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\CompanyTransformerInterface;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\UserCompanyTransformerInterface;

class UserCompanyTransformer implements UserCompanyTransformerInterface
{
    private CompanyTransformerInterface $companyTransformer;

    /**
     * @param \Katamai\wFirmaSdk\Transformers\Commons\Contracts\CompanyTransformerInterface $companyTransformer
     */
    public function __construct(CompanyTransformerInterface $companyTransformer)
    {
        $this->companyTransformer = $companyTransformer;
    }

    public function transform($data): UserCompanyInterface
    {
        wFirmaAssertion::isArray($data);
        wFirmaAssertion::keyExists($data, 'user_company');

        $data['user_company']['company'] = $this->companyTransformer->transform($data['user_company']['company']);

        return new UserCompany($data['user_company']);
    }
}
