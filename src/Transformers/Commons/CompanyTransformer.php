<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Commons;

use Katamai\wFirmaSdk\Extensions\wFirmaAssertion;
use Katamai\wFirmaSdk\Models\Commons\Company;
use Katamai\wFirmaSdk\Models\Commons\Contracts\CompanyInterface;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\CompanyTransformerInterface;

class CompanyTransformer implements CompanyTransformerInterface
{
    public function transform($data): CompanyInterface
    {
        wFirmaAssertion::isArray($data);

        return new Company($data);
    }
}
