<?php
declare(strict_types=1);

namespace Katamai\wFirmaSdk\Transformers\Commons;

use Katamai\wFirmaSdk\Extensions\wFirmaAssertion;
use Katamai\wFirmaSdk\Models\Commons\Contracts\PaginationInterface;
use Katamai\wFirmaSdk\Models\Commons\Pagination;
use Katamai\wFirmaSdk\Transformers\Commons\Contracts\PaginationTransformerInterface;

class PaginationTransformer implements PaginationTransformerInterface
{
    public function transform($data): PaginationInterface
    {
        wFirmaAssertion::isArray($data);

        return new Pagination($data);
    }
}
